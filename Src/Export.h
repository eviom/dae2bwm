#ifndef __EXPORT__
#define __EXPORT__

#include "Common.h"

SAttribute fvector_attribute(const char* name, int& offset);
SAttribute weight_attribute(int joint, int& offset);
SAttribute texcoord_attribute(int& offset);
void write_geometries();
void write_joints();
void write_animation(int begin, int end, float fps, bool looped);
void write_animations(const char* list_file);

#endif
