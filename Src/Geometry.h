#ifndef __GEOMETRY__
#define __GEOMETRY__

#include "Common.h"

void parse_effect(TiXmlElement* _effect);
void parse_material(TiXmlElement* _material);
void parse_vertices(TiXmlElement* verticies);
void parse_polylist(TiXmlElement* polylist, geometry_t* geometry);
void parse_geometry(TiXmlElement* geometry);

#endif
