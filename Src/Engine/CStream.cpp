﻿#include "CStream.h"

#define WRITE(type)									\
CStream& operator <<(CStream& Stream, type& _Data)	\
{													\
	type Data = _Data;								\
	Stream.Write((void*) &Data, sizeof Data);		\
	return Stream;									\
}
#define READ(type)									\
CStream& operator >>(CStream& Stream, type& Data)	\
{													\
	Stream.Read((void*) &Data, sizeof Data);		\
	return Stream;									\
}
#define READWRITE(type) WRITE(type) READ(type)

READWRITE(char)
READWRITE(byte)
READWRITE(short)
READWRITE(unsigned short)
READWRITE(int)
READWRITE(unsigned int)
READWRITE(bool)
READWRITE(float)

#undef READ
#undef WRITE
#undef READWRITE

CStream::~CStream()
{
}
