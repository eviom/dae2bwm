﻿#ifndef SVECTOR
#define SVECTOR

#include "CStream.h"
#include <cmath>

template <typename T>
struct SVector;

typedef SVector<int>   SIVector;
typedef SVector<float> SFVector;

template <typename T>
struct SVector
{
	T x;
	T y;
	T z;

	SVector()
		: x ((T) 0), y ((T) 0), z ((T) 0)
	{}

	explicit SVector (const T a)
		: x (a), y (a), z (a)
	{}

	SVector (const T xx, const T yy, const T zz)
		: x (xx), y (yy), z (zz)
	{}

	SVector<T>& operator += (const SVector<T>& Vector)
	{
		x += Vector.x;
		y += Vector.y;
		z += Vector.z;

		return *this;
	}

	SVector<T>& operator -= (const SVector<T>& Vector)
	{
		x -= Vector.x;
		y -= Vector.y;
		z -= Vector.z;

		return *this;
	}

	SVector<T>& operator *= (const float N)
	{
		x = (T) (x * N);
		y = (T) (y * N);
		z = (T) (z * N);

		return *this;
	}

	SVector<T>& operator /= (const float N)
	{
		x = (T) (x / N);
		y = (T) (y / N);
		z = (T) (z / N);

		return *this;
	}

	void operator = (const SVector<T>& Vector)
	{
		x = Vector.x;
		y = Vector.y;
		z = Vector.z;
	}

	T SqLen() const
	{
		return x * x + y * y + z * z;
	}	

	bool InsideOrOnAABB (const SVector<T>& A, const SVector<T>& B) const
	{
		SVector<T> AP = *this - A;
		SVector<T> BP = *this - B;

		return (AP.x * BP.x <= 0) && (AP.y * BP.y <= 0) && (AP.z * BP.z <= 0);
	}

	SVector<T> operator + (const SVector<T>& Vector) const
	{
		return SVector<T>(x + Vector.x, y + Vector.y, z + Vector.z);
	}

	SVector<T> operator - (const SVector<T>& Vector) const
	{
		return SVector<T>(x - Vector.x, y - Vector.y, z - Vector.z);
	}
	
	SVector<T> operator - () const
	{
		return SVector<T> (-x, -y, -z);
	}

	SVector<T> operator * (const SVector<T>& Vector) const
	{
		return SVector<T> (y * Vector.z - z * Vector.y, 
						   z * Vector.x - x * Vector.z, 
						   x * Vector.y - y * Vector.x);
	}

	SVector<T> operator * (const T N) const
	{
		return SVector<T> ((T) (x * N), (T) (y * N), (T) (z * N));
	}
	
	SVector<T> operator / (const T N) const
	{
		return SVector<T> ((T) (x / N), (T) (y / N), (T) (z / N));
	}

	bool operator == (const SVector<T>& Vector) const
	{
		return x == Vector.x && y == Vector.y && z == Vector.z;
	}

	bool operator != (const SVector<T>& Vector) const
	{
		return x != Vector.x || y != Vector.y || z != Vector.z;
	}

	void operator () (T xx, T yy, T zz)
	{
		x = xx;
		y = yy;
		z = zz;
	}
	
	SVector<T> Normalize()
	{
		T Len = (T) sqrt ((float) SqLen());
		return (*this) / Len;
	}

	template <typename T2>
	operator SVector<T2> () const
	{
		return SVector<T2> ((T2) x, (T2) y, (T2) z);
	}

	T Dot (const SVector<T>& Vector) const
	{
		return x * Vector.x + y * Vector.y + z * Vector.z;
	}

	SVector<T> Abs() const
	{
		return SVector<T> (x >= 0 ? x : -x, y >= 0 ? y : -y, z >= 0 ? z : -z);
	}

	SIVector Sign() const
	{
 		return SIVector (
 			((float) x) < 0.0f ? -1 : 1,
 			((float) y) < 0.0f ? -1 : 1,
 			((float) z) < 0.0f ? -1 : 1);
	}
	
	SVector<T> PerComponentProduct (const SVector<T> Vector) const
	{
		return SVector<T> (x * Vector.x, y * Vector.y, z * Vector.z);
	}
	
};

template<typename T>
CStream& operator <<(CStream& Stream, SVector<T>& Vector)
{
	Stream << Vector.x;
	Stream << Vector.y;
	Stream << Vector.z;

	return Stream;
}

template<typename T>
CStream& operator >>(CStream& Stream, SVector<T>& Vector)
{
	Stream >> Vector.x;
	Stream >> Vector.y;
	Stream >> Vector.z;

	return Stream;
}

#endif
