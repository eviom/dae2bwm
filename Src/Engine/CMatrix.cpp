﻿#include "CMatrix.h"

SFVector operator *(CMatrix<4, 4> M, SFVector V)
{
	CMatrix<4, 1> VMatrix;

	VMatrix(0, 0) = V.x;
	VMatrix(1, 0) = V.y;
	VMatrix(2, 0) = V.z;
	VMatrix(3, 0) = 1.0f;

	VMatrix = M * VMatrix;
	assert (VMatrix (3, 0) == 1.0f);

	return SFVector(VMatrix (0, 0), VMatrix (1, 0), VMatrix (2, 0));
}
