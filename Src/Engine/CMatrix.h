﻿#ifndef CMATRIX
#define CMATRIX

#include <cassert>
#include <algorithm>
#include "SVector.h"
#include "SVector2D.h"

#define static_assert(a, b) assert(a)
#define __assert assert

template<int m, int n> class CMatrix
{
private:
	float Data_[m * n];

public:
	template<int q>
	CMatrix<m, q> operator *(CMatrix<n, q>& B)
	{
		CMatrix<m, n>& A = *this;
		CMatrix<m, q> C;

		for (int i = 0; i < m; i++)
			for (int j = 0; j < q; j++)
			{
				C(i, j) = 0;
				for (int r = 0; r < n; r++)
					C(i, j) += A(i, r) * B(r, j);

			}
		return C;
	}
	template<int q>
	CMatrix<m, n> operator +(CMatrix<m, n>& B)
	{
		CMatrix<m, n>& A = *this;
		CMatrix<m, q> C;

		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
			{
				C(i, j) = A(i, j) + B(i, j);

			}
		return C;
	}
	bool operator ==(CMatrix<m, n>& B)
	{
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
			{
				float d = (*this)(i, j) - B(i, j);
				if (d > 0.000001 || d < -0.000001)
					return false;
			}
					
		return true;
	}
	bool operator !=(CMatrix<m, n>& B)
	{
		return !((*this) == B);
	}
	template<int q>
	CMatrix<m, n> operator *(float B)
	{
		CMatrix<m, n>& A = *this;
		CMatrix<m, q> C;

		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
			{
				C(i, j) = A(i, j) * B;

			}
		return C;
	}
	float& operator ()(int i, int j)
	{
		assert(i < m);
		assert(j < n);
		return Data_[j * n + i];
	}
	void LoadIdentity()
	{
		static_assert (m == n, "Load indentity function created for not square matrix");
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
			{
				(*this)(i, j) = (i == j) ? 1.0f : 0.0f;
			}
	}
	void LoadZero()
	{
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
			{
				(*this)(i, j) = 0.0f;
			}
	}
	float* Data()
	{
		return Data_;
	}
};

SFVector operator *(CMatrix<4, 4> M, SFVector v);

template<int m, int n>
CStream& operator << (CStream& Stream, CMatrix<m, n>& Matrix)
{
	for (int i = 0; i < m * n; i++)
		Stream << Matrix.Data()[i];
		
	return Stream;
}

template<int m, int n>
CStream& operator >> (CStream& Stream, CMatrix<m, n>& Matrix)
{
	for (int i = 0; i < m * n; i++)
		Stream >> Matrix.Data()[i];
		
	return Stream;
}

#endif
