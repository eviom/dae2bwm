﻿#include "CString.h"

CString CString::EmptyString = "";

CString::CString() :
Length_(0)
{
	Data_[0] = '\0';
}

CString::CString(const char* String)
{
	int i = 0;
	for (; String[i]; i++)
	{
		assert(i < MAX_STR_SIZE);
		Data_[i] = String[i];
	}
	Length_ = i;
	Data_[Length_] = 0;
}

CString::CString(const char* String, int Start, int End)
{
	for (int i = Start; i < End; i++)
		Data_[i - Start] = String[i];

	Length_ = End - Start;
	Data_[Length_] = 0;
}

CString::CString(CString& String, int Start, int End)
{
	for (int i = Start; i < End; i++)
		Data_[i - Start] = String[i];

	Length_ = End - Start;
	Data_[Length_] = 0;
}

void CString::Print (const char* Format, ...)
{
	va_list Args;
	va_start(Args, Format);
	VPrint (Format, Args);
	va_end(Args);
}

void CString::VPrint (const char* Format, va_list Args)
{
	vsnprintf(Data_, MAX_STR_SIZE - 1, Format, Args);
	Length_ = strlen(Data_);
	assert(Length_ < MAX_STR_SIZE);
}

void CString::Append (CString& String)
{
	strncat (Data_, String.Data(), MAX_STR_SIZE - Length_ - 1);

	Length_ += String.Length();
	Data_[Length_] = 0;
}

void CString::Shorten(int Length)
{
	assert (Length <= Length_);
	Length_ = Length;
	Data_[Length_] = 0;
}

char& CString::operator[](int i)
{
	assert(i >= 0 && i < Length_);
	return Data_[i];
}

void CString::operator +=(char c)
{
	assert(Length_ < MAX_STR_SIZE - 1);
	Data_[Length_] = c;
	Length_++;
	Data_[Length_] = '\0';
}

bool CString::operator == (CString& String)
{
	if (Length_ != String.Length_)
		return false;

	for (int i = 0; i < Length_; i++)
	{
		if (Data_[i] != String[i])
			return false;
	}

	return true;
}

void CString::operator = (const char* String)
{
	strcpy (Data_, String);
	Length_ = strlen (String);
	assert(Length_ < MAX_STR_SIZE);
	Data_[Length_] = '\0';
}

int CString::Length()
{
	return Length_;
}

bool CString::Empty()
{
	return !Length_;
}

char* CString::Data()
{
	return Data_;
}

void CString::Clear()
{
	Shorten(0);
}

int CString::Hash (CString& String)
{
	int Hash = 0;

	for (int i = 0; i < String.Length(); i++)
		Hash = (Hash << 5) + Hash + String[i];

	return Hash;
}

CStream& operator <<(CStream& Stream, CString& String)
{
	int Length = String.Length();
	Stream << Length;

	for (int i = 0; i < Length; i++)
		Stream << String.Data()[i];

	return Stream;
}

CStream& operator >>(CStream& Stream, CString& String)
{
	int Length;
	Stream >> Length;

	String.Clear();
	for (int i = 0; i < Length; i++)
	{
		char c;
		Stream >> c;
		String += c;
	}

	return Stream;
}
