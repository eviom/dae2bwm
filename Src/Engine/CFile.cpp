﻿#include "CFile.h"

CFile::CFile() :
File_ (NULL),
Mode_(-1)
{
}

void ToGameFolderPath (const char* FileName, char* GameFolderPath)
{
	strcpy(GameFolderPath, FileName);
}

CFile::CFile (const char* FileName, int Mode, bool FileMustExist) 
{
	Init (FileName, Mode, FileMustExist);
}

void CFile::Init (const char* FileName, int Mode, bool FileMustExist)
{
	Mode_ = Mode;

	char GameFolderPath[MAX_PATH] = "";
	ToGameFolderPath (FileName, GameFolderPath);
	
	CArray<char> ModeArray (4);
	bool RW = ((Mode & CFILE_READ) && (Mode & CFILE_WRITE));
	
	if (!RW)
	{
		if (Mode & CFILE_APPEND)
			ModeArray.Insert ('a');
		else if (Mode & CFILE_WRITE)
			ModeArray.Insert ('w');
		else if (Mode & CFILE_READ)
			ModeArray.Insert ('r');
	}
	else
	{
	   if (Mode & CFILE_CREATE)
			ModeArray.Insert ('w');
	   else
			ModeArray.Insert ('r');
	   ModeArray.Insert ('+');

	}

   if (Mode & CFILE_BINARY)
	   ModeArray.Insert ('b');

	ModeArray.Insert (0);
	assert (ModeArray.Size() != 0);

	File_ = fopen (GameFolderPath, ModeArray.Data());

	if (FileMustExist && !File_)
	{
		assert(false);
	}
}

void CFile::Destroy()
{
	assert (File_);
	fclose (File_);
	File_ = NULL;
}

CFile::~CFile()
{
	//assert (!File_);
}

void CFile::Read(void* Data, int Size)
{
	assert (File_);
	assert (Mode_ & CFILE_READ);
	assert (fread (Data, Size, 1, File_) == 1);
}

void CFile::Write(void* Data, int Size)
{
	assert (File_);
	assert (Mode_ & CFILE_WRITE);
	assert (fwrite (Data, Size, 1, File_) == 1);
}

CFile::operator bool ()
{
	return IsFile();
}

bool CFile::operator ! ()
{
	return !IsFile();
}

bool CFile::IsFile()
{
	return File_ != NULL;
}

FILE* CFile::GetFILE()
{
	assert (File_);
	return File_;
}

bool CFile::EndOfFile()
{
	assert (File_);
	return feof (File_) != 0;
}

void CFile::Seek (int Offset, int Origin)
{
	assert (File_);
	assert (fseek (File_, Offset, Origin) == 0);
}

long CFile::Tell()
{
	assert (File_);
	return ftell (File_);
}

void CFile::Flush()
{
	assert (File_);
	assert (fflush (File_) == 0);
}

bool CFile::GetString (char* String, int MaxSize, bool CheckError)
{	
	assert (File_);
	char* RetValue = fgets (String, MaxSize, File_);
	if (CheckError)
	{
		assert (RetValue == String || feof (File_));
		return true;
	}
	else
		return RetValue == String;
}

void CFile::Scan (const char* Format, ...)
{
	va_list Args;
	va_start(Args, Format);
	VScan (Format, Args);
	va_end(Args);
}

void CFile::VScan (const char* Format, va_list Args)
{
	assert (File_);
	assert (vfscanf (File_, Format, Args) >= 0);
}

void CFile::Print (const char* Format, ...)
{
	va_list Args;
	va_start(Args, Format);
	VPrint (Format, Args);
	va_end(Args);
}

void CFile::VPrint (const char* Format, va_list Args)
{
	assert (File_);
	assert (vfprintf (File_, Format, Args) >= 0);
}
