﻿#ifndef CFILE
#define CFILE

#include "CStream.h"
#include "CArray.h"
#include <cstdio>
#include <cstdarg>
#include <cassert>
#include <cstring>

#define MAX_PATH 256

enum CFileModes
{
	CFILE_READ		= 1, 
	CFILE_WRITE		= 2, 
	CFILE_BINARY	= 4, 
	CFILE_APPEND	= 8, 
	CFILE_CREATE	= 16
};

class CFile : public CStream
{
private:

	FILE*	File_;
	int		Mode_;

public:
	
	//CFile (const CFile& Other);
	//CFile& operator = (const CFile& Other);

	CFile();
	CFile (const char* FileName, int Mode, bool FileMustExist = true);
	~CFile();
	
	void Init (const char* FileName, int Mode, bool FileMustExist = true);
	void Destroy();

	virtual void Read (void* Data, int Size);
	virtual void Write (void* Data, int Size);

	operator bool ();
	bool operator ! ();
	bool IsFile();
	FILE* GetFILE();
	bool EndOfFile();
	void Seek (int Offset, int Origin);
	long Tell();
	void Flush();

	bool GetString (char* String, int MaxSize, bool CheckError = true);
	void Scan   (const char* Format, ...);
	void VScan  (const char* Format, va_list Args);
	void Print  (const char* Format, ...);
	void VPrint (const char* Format, va_list Args);

};

#endif
