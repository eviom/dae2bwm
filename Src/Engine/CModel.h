#ifndef __CMODEL__
#define __CMODEL__

#include "CArray.h"
#include "CMatrix.h"
#include "CFile.h"
#include "CString.h"

/*vertex data outline:
 * vertex
 * (color)
 * (texture coordinates)
 * (normals)
 * <specific data>
 * weights
 */ 
 
struct SAttribute
{
	CString						Name;
	int							Components;
	int							Type;
	int							Offset;
};

struct SInfluence
{
	int             			Joint;
	CMatrix<4,4>    			InvBind;
};

struct SMesh
{
	bool						Texture;
	SVector<float>				Color;
	CArray<SAttribute>			Attributes;
	CArray<SInfluence>			Joints;
	CArray<byte>				Data;
	int							NVertices;
};

struct SJoint
{
	int							Parent;
	CMatrix<4, 4>				Transformation;
};

typedef CArray<CMatrix<4, 4> >	SAnimationFrame;

struct SAnimation
{
	float						FPS;
	CArray<int>					ActiveJoints;
	CArray<SAnimationFrame>		Frames;
	bool						Looped;
};

struct SModel
{
	CString						Texture;
	CArray<SMesh>				Meshes;
	CArray<SJoint>				Joints;
	CArray<SAnimation>			Animations;
};

CStream& operator << (CStream& Stream, SModel& Model);
CStream& operator >> (CStream& Stream, SModel& Model);
CStream& operator << (CStream& Stream, SAnimation& Animation);
CStream& operator >> (CStream& Stream, SAnimation& Animation);
CStream& operator << (CStream& Stream, SJoint& Joint);
CStream& operator >> (CStream& Stream, SJoint& Joint);
CStream& operator << (CStream& Stream, SMesh& Mesh);
CStream& operator >> (CStream& Stream, SMesh& Mesh);
CStream& operator << (CStream& Stream, SAttribute& Attribute);
CStream& operator >> (CStream& Stream, SAttribute& Attribute);
CStream& operator << (CStream& Stream, SInfluence& Influence);
CStream& operator >> (CStream& Stream, SInfluence& Influence);


template<typename T>
CArray<byte>& operator << (CArray<byte>& Array, T Data)
{
	assert(sizeof Data <= 4); //to avoid classes...
	
	byte* _Data = (byte*) &Data;
	
	for (int i = 0; i < sizeof Data; i++)
		Array.Insert(_Data[i], true);
	
	return Array;
}

#endif
