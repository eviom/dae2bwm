﻿#ifndef CSTREAM
#define CSTREAM

typedef unsigned char byte;

class CStream
{
public:

	virtual void Read(void* Data, int Size) = 0;
	virtual void Write(void* Data, int Size) = 0;

	virtual ~CStream();

};


#define WRITE(type)	CStream& operator <<(CStream& Stream, type& _Data);
#define READ(type)  CStream& operator >>(CStream& Stream, type& Data);
#define READWRITE(type) WRITE(type) READ(type)

READWRITE(char)
READWRITE(byte)
READWRITE(short)
READWRITE(unsigned short)
READWRITE(int)
READWRITE(unsigned int)
READWRITE(bool)
READWRITE(float)

#undef READ
#undef WRITE
#undef READWRITE

#endif
