#include "CModel.h"

CStream& operator << (CStream& Stream, SModel& Model)
{
	return Stream << Model.Texture << Model.Meshes << Model.Joints << Model.Animations;
}
CStream& operator >> (CStream& Stream, SModel& Model)
{
	return Stream >> Model.Texture >> Model.Meshes >> Model.Joints >> Model.Animations;
}
CStream& operator << (CStream& Stream, SAnimation& Animation)
{
	return Stream << Animation.FPS << Animation.ActiveJoints << Animation.Frames << Animation.Looped;
}
CStream& operator >> (CStream& Stream, SAnimation& Animation)
{
	return Stream >> Animation.FPS >> Animation.ActiveJoints >> Animation.Frames >> Animation.Looped;
}
CStream& operator << (CStream& Stream, SJoint& Joint)
{
	return Stream << Joint.Parent << Joint.Transformation;
}
CStream& operator >> (CStream& Stream, SJoint& Joint)
{
	return Stream >> Joint.Parent >> Joint.Transformation;
}
CStream& operator << (CStream& Stream, SMesh& Mesh)
{
	return Stream << Mesh.Texture << Mesh.Color << Mesh.Attributes << Mesh.Joints << Mesh.Data << Mesh.NVertices;
}
CStream& operator >> (CStream& Stream, SMesh& Mesh)
{
	return Stream >> Mesh.Texture >> Mesh.Color >> Mesh.Attributes >> Mesh.Joints >> Mesh.Data >> Mesh.NVertices;
}
CStream& operator << (CStream& Stream, SAttribute& Attribute)
{
	return Stream << Attribute.Name << Attribute.Components << Attribute.Type << Attribute.Offset;
}
CStream& operator >> (CStream& Stream, SAttribute& Attribute)
{
	return Stream >> Attribute.Name >> Attribute.Components >> Attribute.Type >> Attribute.Offset;
}
CStream& operator << (CStream& Stream, SInfluence& Influence)
{
	return Stream << Influence.Joint << Influence.InvBind;
}
CStream& operator >> (CStream& Stream, SInfluence& Influence)
{
	return Stream >> Influence.Joint >> Influence.InvBind;
}
