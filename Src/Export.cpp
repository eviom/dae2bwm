#include "Export.h"

SAttribute fvector_attribute(const char* name, int& offset)
{
	SAttribute Attribute;
	Attribute.Name       = name;
	Attribute.Components = 3;
	Attribute.Type       = TYPE_FLOAT;
	Attribute.Offset     = offset;
	
	offset += 3 * 4;
	return Attribute;
}

SAttribute weight_attribute(int joint, int& offset)
{
	SAttribute Attribute;
	Attribute.Name.Print("JointWeight%d", joint);
	Attribute.Components = 1;
	Attribute.Type       = TYPE_FLOAT;
	Attribute.Offset     = offset;
	
	offset += 4;
	return Attribute;
}

SAttribute texcoord_attribute(int& offset)
{
	SAttribute Attribute;
	Attribute.Name.Print("Vertex_TextureCoord");
	Attribute.Components = 2;
	Attribute.Type       = TYPE_FLOAT;
	Attribute.Offset     = offset;
	
	offset += 2 * 4;
	return Attribute;
}

void write_geometries()
{
	for (map<string, geometry_t>::iterator m = geometries.begin(); m != geometries.end(); m++)
	{
		geometry_t& geometry = m->second;
		
		for (int j = 0; j < (int) geometry.meshes.size(); j++)
		{
			Model.Meshes.Resize(Model.Meshes.Size() + 1);
			Model.Meshes.SetStatic();
			
			SMesh&  Mesh = Model.Meshes[Model.Meshes.Size() - 1];
			mesh_t& mesh = geometry.meshes[j];
			
			int offset = 0;
			
			Mesh.Attributes.Insert(fvector_attribute("Vertex_Position", offset), true);
			Mesh.Attributes.Insert(fvector_attribute("Vertex_Normal", offset), true);
			
			if (mesh.texture)
				Mesh.Attributes.Insert(texcoord_attribute(offset), true);
				
			for (int i = 0; i < (int) geometry.joints.size(); i++)
			{
				Mesh.Attributes.Insert(weight_attribute(i, offset), true);
				
				SInfluence Joint;
				Joint.Joint   = get_joint(geometry.joints[i].joint).index;
				Joint.InvBind = geometry.joints[i].inv_bind;
				Mesh.Joints.Insert(Joint, true);
			}
		
			for (int i = mesh.begin_index; i < (int) mesh.end_index; i++)
			{
				Mesh.Data << geometry.verteces[i].Position.x << geometry.verteces[i].Position.y << geometry.verteces[i].Position.z;
				Mesh.Data << geometry.verteces[i].Normal.x   << geometry.verteces[i].Normal.y   << geometry.verteces[i].Normal.z;
				
				if (mesh.texture)
					Mesh.Data << geometry.verteces[i].TexCoord.x << geometry.verteces[i].TexCoord.y;
				
				for (int k = 0; k < (int) geometry.joints.size(); k++)
					Mesh.Data << geometry.weights[i * geometry.joints.size() + k];
			}
			
			string mesh_name = m->first;
			
			if (mesh_name == "Minifig_Head-mesh" || mesh_name == "Minifig_Hand_R-mesh" || mesh_name == "Minifig_Hand_L-mesh")
				Mesh.Color = SFVector(1.0f, 0.9f, 0.4f);
			else if (mesh_name == "Minifig_Torso-mesh" || mesh_name == "Minifig_Arm_R-mesh" || mesh_name == "Minifig_Arm_L-mesh")
				Mesh.Color = SFVector(1.0f, 1.0f, 1.0f);
			else
				Mesh.Color = mesh.material.color;
			
			Mesh.Texture   = mesh.texture;
			Mesh.NVertices = mesh.end_index - mesh.begin_index;
		}
	}
	
	Model.Texture = texture;
}

void write_joints()
{
	for (int i = 0; i < (int) joints_list.size(); i++)
	{
		joint_t& joint = get_joint(joints_list[i]);
		
		SJoint Joint;
		Joint.Parent         = joint.parent;
		Joint.Transformation = joint.transformation;
		
		Model.Joints.Insert(Joint, true);
	}
	 
}

void write_animation(int begin, int end, float fps, bool looped)
{
	Model.Animations.Resize(Model.Animations.Size() + 1);
	Model.Animations.SetStatic();
	SAnimation& Animation = Model.Animations[Model.Animations.Size() - 1];
	
	CArray<int>& Active = Animation.ActiveJoints;
	
	for (int i = 0; i < (int) joints_list.size(); i++)
	{
		bool IsActive = false;
		
		for (int j = begin; j <= end; j++)
		{
			if (animation[i][j] != joints.at(joints_list[i]).transformation)
				IsActive = true;
		}
		
		if (IsActive)			
			Active.Insert(i, true);
	}
	
	Animation.FPS    = fps;
	Animation.Looped = looped;
	
	Animation.Frames.Resize(end - begin + 1);
	Animation.Frames.SetStatic();
	
	printf("animation: FPS=%f, frames=%d, n_active=%d\n", Animation.FPS, Animation.Frames.Size(), Animation.ActiveJoints.Size());
	for (int i = 0; i < Active.Size(); i++)
		printf("\t-%s\n", joints_list[Active[i]].c_str());
		
	for (int i = 0; i < Animation.Frames.Size(); i++)
	{
		Animation.Frames[i].Resize(Active.Size());
		Animation.Frames[i].SetStatic();
		
		for (int j = 0; j < Active.Size(); j++)
			Animation.Frames[i][j] = animation[Active[j]][begin + i];
	}
}

void write_animations(const char* list_file)
{
	FILE* list = fopen(list_file, "rt");
	
	int begin, end, looped;
	float fps;
	char name[80];
	
	while (fscanf(list, "%s%d%d%f%d", name, &begin, &end, &fps, &looped) == 5)
		write_animation(begin, end, fps, looped);
		
	fclose(list);
}
