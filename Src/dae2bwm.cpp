#include "dae2bwm.h"

map<string, vector<float> >   		 float_arrays;
map<string, vector<string> >         NAME_arrays;
map<string, source_t>				 sources;
map<string, joint_t>  				 joints;
map<string, geometry_t>				 geometries;
map<string, sampler_t>               samplers;
map<string, effect_t>                effects;
map<string, material_t>              materials;

TiXmlElement*     					root_joint = NULL;
vector<sampler_t>					animation;
vector<string>						joints_list;

SModel Model;
CFile out;
CString texture;

void strip_sharp(string* str)
{
	assert((*str)[0] == '#');
	str->erase(0, 1);
}

joint_t& get_joint(string sid)
{
	return joints.at(sid);
}

vector<float>& get_float_array(string id)
{
	strip_sharp(&id);
	return float_arrays.at(id);
}

vector<string>& get_NAME_array(string id)
{
	strip_sharp(&id);
	return NAME_arrays.at(id);
}

source_t& get_source(string id)
{
	strip_sharp(&id);
	return sources.at(id);
}

geometry_t& get_geomery(string id)
{
	strip_sharp(&id);
	return geometries.at(id);
}

sampler_t& get_sampler(string id)
{
	strip_sharp(&id);
	return samplers.at(id);
}

effect_t& get_effect(string id)
{
	strip_sharp(&id);
	return effects.at(id);
}

material_t& get_material(string id)
{
	strip_sharp(&id);
	return materials.at(id);
}

float get_param_float(source_t& source, int index, string name)
{
	vector<float>& array  = get_float_array(source.source);
	
	assert(!strcasecmp(source.params.at(name).type.c_str(), "float"));
	
	int i = source.stride * index + source.params.at(name).offset;
	return array[i];
}

float get_param_float(string id, int index, string name)
{
	return get_param_float(get_source(id), index, name);
}

string get_param_name(source_t& source, int index, string name)
{
	vector<string>& array = get_NAME_array(source.source);
	
	assert(!strcasecmp(source.params.at(name).type.c_str(), "name"));
	
	int i = source.stride * index + source.params.at(name).offset;
	return array[i];
}

string get_param_name(string id, int index, string name)
{
	return get_param_name(get_source(id), index, name);
}

CMatrix<4, 4> get_param_float4x4(source_t& source, int index, string name)
{
	printf("==========%s %d\n", name.c_str(), index);
	vector<float>& array  = get_float_array(source.source);
	
	assert(source.params.find(name) != source.params.end());
	assert(!strcasecmp(source.params.at(name).type.c_str(), "float4x4"));
	
	int k = source.stride * index + source.params.at(name).offset;
	CMatrix<4, 4> Matrix;
	
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
		{
			Matrix(i, j) = array[k];
			k++;
		}
		
	for (int j = 0; j < 4; j++)
	{
		assert(Matrix(3, j) == ((j == 3) ? 1 : 0));
	}
		
	return Matrix;
}

CMatrix<4, 4> get_param_float4x4(string id, int index, string name)
{
	return get_param_float4x4(get_source(id), index, name);
}

void get_array_data(TiXmlElement* array, string* id, int* count, const char** data)
{	
	assert(array->QueryStringAttribute("id",    id   ) == TIXML_SUCCESS);
	assert(array->QueryIntAttribute   ("count", count) == TIXML_SUCCESS);
	
	TiXmlNode* text = array->FirstChild();
	assert(text && text->Type() == TiXmlNode::TINYXML_TEXT && !text->NextSibling());
	
	assert(*data = text->Value());
	assert(float_arrays.find(*id) == float_arrays.end());
	assert(NAME_arrays.find(*id)  == NAME_arrays.end());
	
	printf("%s\t%s\t%d\n", array->Value(), id->c_str(), *count);
}
