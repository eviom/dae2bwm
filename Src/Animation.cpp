#include "Animation.h"

void find_root_joint(TiXmlNode* node)
{
	for (TiXmlNode* child = node->FirstChild(); child; child = child->NextSibling())
	{
		bool Suitable = false;
		
		if (child->Type() == TiXmlNode::TINYXML_ELEMENT && !strcasecmp(child->Value(), "node"))
		{
			TiXmlElement* _child = (TiXmlElement*) child;
			const char* type = _child->Attribute("type");
			
			if (type && !strcasecmp(type, "JOINT"))
			{
				printf("%s\n", _child->Attribute("id"));
				assert(!root_joint);
				root_joint = _child;
				Suitable = true;
			}
		}
		
		if (!Suitable)
			find_root_joint(child);
	}
}

void read_skeleton(TiXmlElement* joint, int parent)
{
	/*do not want to care about multiple joint sid's: 1 tree anyway*/
	const char* sid  = joint->Attribute("sid");
	const char* type = joint->Attribute("type");
	assert(sid);
	assert(type);
	assert(!strcasecmp(type, "JOINT"));
	assert(joints.find(sid) == joints.end());
	
	TiXmlElement* matrix = joint->FirstChildElement("matrix");
	assert(matrix);
	const char* matrix_sid =    matrix->Attribute("sid");
	assert(matrix_sid && !strcasecmp(matrix_sid, "transform"));
	TiXmlNode* matrix_data = matrix->FirstChild();
	assert(matrix_data && matrix_data->Type() == TiXmlNode::TINYXML_TEXT);
	
	joints[sid].index          = joints_list.size();
	joints[sid].parent         = parent;
	joints[sid].transformation = read_matrix(matrix_data->Value());
	
	joints_list.push_back(sid);
	
	printf("%d %d %s\n", joints[sid].parent, joints[sid].index, sid);
	
	for (TiXmlElement* child = joint->FirstChildElement("node"); child; child = child->NextSiblingElement("node"))
		read_skeleton(child, joints[sid].index);
	
}

void parse_skin(TiXmlElement* skin)
{
	string _mesh;
	assert(skin->QueryStringAttribute("source", &_mesh) == TIXML_SUCCESS);
	geometry_t& mesh = get_geomery(_mesh);
	
	TiXmlElement* _bind_shape = skin->FirstChildElement("bind_shape_matrix");
	assert(_bind_shape);
	TiXmlNode* bind_shape_text = _bind_shape->FirstChild();
	assert(bind_shape_text && bind_shape_text->Type() == TiXmlNode::TINYXML_TEXT);
	CMatrix<4,4> bind_shape = read_matrix(bind_shape_text->Value());
	
	TiXmlElement* _joints = skin->FirstChildElement("joints");
	assert(_joints);
	
	bool joints_read = false, matrices_read = false;
	string joint_source;
	vector<string> joint_sids;	
	for (TiXmlElement* input = _joints->FirstChildElement("input"); input; input = input->NextSiblingElement("input"))
	{	
		string semantic;				
		assert(input->QueryStringAttribute("semantic", &semantic) == TIXML_SUCCESS);
			
		if (!strcasecmp(semantic.c_str(), "JOINT"))
		{
			string _source;
			assert(!joints_read);
			joints_read = true;
			assert(input->QueryStringAttribute("source", &_source) == TIXML_SUCCESS);
			source_t& source = get_source(_source);
			joint_source = _source;
			
			for (int i = 0; i < source.count; i++)
			{
				mesh.joints.resize(i + 1);
				string joint = get_param_name(_source, i, "joint");
				joint_sids.push_back(joint);
				mesh.joints[i].joint = joint;
			}
		}
	}		
		
	for (TiXmlElement* input = _joints->FirstChildElement("input"); input; input = input->NextSiblingElement("input"))
	{
		string semantic;				
		assert(input->QueryStringAttribute("semantic", &semantic) == TIXML_SUCCESS);
			
		if (!strcasecmp(semantic.c_str(), "INV_BIND_MATRIX"))
		{
			string _source;
			string semantic;
				
			assert(input->QueryStringAttribute("semantic", &semantic) == TIXML_SUCCESS);
			assert(!matrices_read);
			matrices_read = true;
			assert(input->QueryStringAttribute("source", &_source) == TIXML_SUCCESS);
			source_t& source = get_source(_source);
			assert(source.count == (int) mesh.joints.size());
			
			for (int i = 0; i < source.count; i++)
			{
				CMatrix<4, 4> inv_bind       = get_param_float4x4(_source, i, "transform");
				mesh.joints[i].inv_bind      = inv_bind;
			}
	
		}
	}
	
	assert(joints_read && matrices_read);
	
	
	TiXmlElement* vertex_weights = skin->FirstChildElement("vertex_weights");
	int count;
	assert(vertex_weights->QueryIntAttribute("count", &count) == TIXML_SUCCESS);
	map<string, input_t> inputs = parse_inputs(vertex_weights);
	
	
	vector<int> vcount;
	{
		TiXmlElement* _vcount = vertex_weights->FirstChildElement("vcount");
		assert(_vcount);
		TiXmlNode* text = _vcount->FirstChild();
		assert(text && text->Type() == TiXmlNode::TINYXML_TEXT);
		const char* data = text->Value();		
		stringstream stream(data);
		
		for (int i = 0; i < count; i++)
		{
			int c;
			assert(stream >> c);
			vcount.push_back(c);
		}
	}

	input_t& weight_input = inputs.at("weight");
	input_t& joint_input  = inputs.at("joint");	
	assert(joint_source == joint_input.source);
	
	TiXmlElement* v = vertex_weights->FirstChildElement("v");		
	assert(v);
	TiXmlNode* text = v->FirstChild();
	assert(text && text->Type() == TiXmlNode::TINYXML_TEXT);
	const char* data = text->Value();		
	stringstream stream(data);
	
	int I;
	vector<int>	indeces;
	while (stream >> I)
		indeces.push_back(I);
	
	
	vector<float> _weights(mesh.joints.size() * count, 0.0f);
	
	int c = 0;
	int abs_count = 0;
	for (int i = 0; i < count; i++)
		for (int j = 0; j < vcount[i]; j++)
			abs_count++;
	int stride = indeces.size() / abs_count;
	
	for (int i = 0; i < count; i++)
	{
		float total_weight = 0.0f;
		
		for (int j = 0; j < vcount[i]; j++)
		{
			int joint_index  = indeces[c + joint_input.offset];
			int weight_index = indeces[c + joint_input.offset];
			c += stride;
			
			float weight = get_param_float(weight_input.source, weight_index, "weight");
			total_weight += weight;
			
			_weights[i * mesh.joints.size() + joint_index] = weight;
		}
		
		
		for (int j = 0; j < (int) mesh.joints.size(); j++)
			_weights[i * mesh.joints.size() + j] /= total_weight;
	}
	
	for (int i = 0; i < (int) mesh.indeces.size(); i++)
	{
		mesh.verteces[i].Position = bind_shape * mesh.verteces[i].Position;
		
		for (int j = 0; j < (int) mesh.joints.size(); j++)
			mesh.weights.push_back(_weights[mesh.indeces[i] * mesh.joints.size() + j]);
	}
}

void parse_sampler(TiXmlElement* sampler)
{	
	string id;
	assert(sampler->QueryStringAttribute("id", &id) == TIXML_SUCCESS);
	assert(samplers.find(id) == samplers.end());
	
	map<string, input_t> inputs = parse_inputs(sampler, false);
	
	source_t& input         = get_source(inputs.at("input").source);
	source_t& output        = get_source(inputs.at("output").source);
	source_t& interpolation = get_source(inputs.at("interpolation").source);
	
	if (!output.params.count("transform"))
		return;
		
	sampler_t& sampler_data = samplers[id];
	
	assert(input.count == output.count && output.count == interpolation.count);
	
	for (int i = 0; i < input.count; i++)
	{
		//float         time      = get_param_float(input, i, "time");
		CMatrix<4, 4> transform = get_param_float4x4(output, i, "transform");
		string        interp    = get_param_name(interpolation, i, "interpolation");
		
		if (strcasecmp(interp.c_str(), "LINEAR"))
			printf("WARNING: only linear interpolation is supported.\n");
		
		sampler_data.push_back(transform);
	}
}

void parse_channel(TiXmlElement* channel)
{
	string source, target;
	assert(channel->QueryStringAttribute("source", &source) == TIXML_SUCCESS);
	assert(channel->QueryStringAttribute("target", &target) == TIXML_SUCCESS);
	char joint_name[80] = {};
	char frame_type[80] = {};
	assert(sscanf(target.c_str(), "%[a-zA-Z0-9_]/%[a-zA-Z0-9_]", joint_name, frame_type) == 2);
	printf("%s\n", target.c_str());
	if (strcasecmp(frame_type, "transform"))
	{
		printf("WARNING: only matrix animation frames are supported (%s)\n", target.c_str());
		return;
	}
	
	int joint = get_joint(joint_name).index;
	//printf("-----%d %d\n", joint, (int) joints.size());
	sampler_t& sampler = get_sampler(source);
	
	//animation[joint];
	animation[joint].insert(animation[joint].end(), sampler.begin(), sampler.end());
}
