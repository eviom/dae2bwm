#include <vector>

using std::vector;

template<typename T>
class _vector : public vector<T>
{
public:
	T& operator[] (int i)
	{
		return this->at(i);
	}
	
	_vector(int size, T init) :
		vector<T>(size, init)
	{
	}
	
	_vector()
	{
	}
	
		
	
	
};

#define vector _vector
