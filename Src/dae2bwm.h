#ifndef __DAE2BWM__
#define __DAE2BWM__

#include <tinyxml.h>
#include <string.h>
#include <assert.h>
#include <map>
#include <string>
#include <sstream>
#include "vector.h"

#include "Engine/CModel.h"

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#define TYPE_FLOAT   0
#define TYPE_BYTE    1

using std::map;
using std::string;
using std::stringstream;

struct joint_t
{
	int					index;
	int					parent;
	CMatrix<4,4>		transformation;
};

struct param_t
{
	string          	type;
	int					offset;
};

struct source_t
{
	string          		source;
	int             		stride;
	map<string, param_t> 	params;
	int             		count;
};

struct input_t
{
	string          	source;
	int             	offset;
};

struct vertex_t
{
	SFVector   		Position;
	SFVector		Normal;
	SFVector2D		TexCoord;
};

struct influence_t
{
	string       joint;
	CMatrix<4,4> inv_bind;
};

struct effect_t
{
	SFVector color;
};

typedef effect_t material_t;

struct mesh_t
{
	bool                   texture;
	material_t             material;
	
	int                    begin_index;
	int                    end_index;
	
};

struct geometry_t
{	
	vector<vertex_t> 		verteces;
	vector<influence_t>     joints;
	vector<float>    		weights;
	
	vector<int>				indeces; //for weights reading
	
	vector<mesh_t>          meshes;
};

typedef vector<CMatrix<4, 4> >       sampler_t;


//TODO: use macro
extern map<string, vector<float> >   		float_arrays;
extern map<string, vector<string> >         NAME_arrays;
extern map<string, source_t>				sources;
extern map<string, joint_t>  				joints;
extern map<string, geometry_t>				geometries;
extern map<string, sampler_t>               samplers;
extern map<string, effect_t>                effects;
extern map<string, material_t>              materials;

extern TiXmlElement*     					root_joint;
extern vector<sampler_t>					animation;
extern vector<string>						joints_list;

extern SModel Model;
extern CFile out;
extern CString texture;


//==============================
void strip_sharp(string* str);
joint_t& get_joint(string sid);
vector<float>& get_float_array(string id);
vector<string>& get_NAME_array(string id);
source_t& get_source(string id);
geometry_t& get_geomery(string id);
sampler_t& get_sampler(string id);
effect_t& get_effect(string id);
material_t& get_material(string id);

float get_param_float(source_t& source, int index, string name);
float get_param_float(string id, int index, string name);
string get_param_name(source_t& source, int index, string name);
string get_param_name(string id, int index, string name);
CMatrix<4, 4> get_param_float4x4(source_t& source, int index, string name);
CMatrix<4, 4> get_param_float4x4(string id, int index, string name);
void get_array_data(TiXmlElement* array, string* id, int* count, const char** data);

#endif
