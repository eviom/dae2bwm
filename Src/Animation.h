#ifndef __ANIMATION__
#define __ANIMATION__

#include "Common.h"

void find_root_joint(TiXmlNode* node);
void read_skeleton(TiXmlElement* joint, int parent);
void parse_skin(TiXmlElement* skin);
void parse_sampler(TiXmlElement* sampler);
void parse_channel(TiXmlElement* channel);

#endif
