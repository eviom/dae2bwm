#ifndef __COMMON__
#define __COMMON__

#include "dae2bwm.h"

void parse_float_array(TiXmlElement* array);
void parse_NAME_array(TiXmlElement* array);
CMatrix<4, 4> read_matrix(const char* data);
map<string, input_t>  parse_inputs(TiXmlElement* data, bool offset = true);
void parse_all(TiXmlNode* node, const char* name, void (*handler)(TiXmlElement*));
void parse_source(TiXmlElement* source);

#endif
