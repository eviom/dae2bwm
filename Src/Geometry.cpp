#include "Geometry.h"

void parse_effect(TiXmlElement* _effect)
{
	string id;
	assert(_effect->QueryStringAttribute("id", &id) == TIXML_SUCCESS);
	
	TiXmlElement* profile_COMMON = _effect->FirstChildElement("profile_COMMON");
	assert(profile_COMMON);
	
	TiXmlElement* technique = profile_COMMON->FirstChildElement("technique");
	assert(technique);
	
	TiXmlElement* parameters = technique->FirstChildElement("phong");
	if (!parameters)
		parameters = technique->FirstChildElement("blin");
		
	assert(parameters);
	
	TiXmlElement* diffuse = parameters->FirstChildElement("diffuse");
	assert(diffuse);
	TiXmlElement* color = diffuse->FirstChildElement("color");
	
	effect_t effect;
		
	if (color)
	{
		TiXmlNode* color_data = color->FirstChild();
		assert(color_data);
		
		assert(color_data->Type() == TiXmlNode::TINYXML_TEXT);
		const char* data = color_data->Value();
		stringstream stream(data);
		
		assert(stream >> effect.color.x >> effect.color.y >> effect.color.z);	
	}
	else
		effect.color = SFVector(1.0f, 1.0f, 1.0f);
	
	assert(!effects.count(id));
	effects[id] = effect;
}

void parse_material(TiXmlElement* _material)
{
	string id;
	assert(_material->QueryStringAttribute("id", &id) == TIXML_SUCCESS);
	
	TiXmlElement* instance_effect = _material->FirstChildElement("instance_effect");
	assert(instance_effect);
	
	string url;
	assert(instance_effect->QueryStringAttribute("url", &url) == TIXML_SUCCESS);
	
	assert(!materials.count(id));	
	materials[id] = get_effect(url);
}

void parse_vertices(TiXmlElement* verticies)
{
	string id, semantic, source;
	assert(verticies->QueryStringAttribute("id",  &id)  == TIXML_SUCCESS);
	
	TiXmlElement* input = verticies->FirstChildElement("input");
	assert(!input->NextSiblingElement("input")); //can happen, in some rare cases

	assert(input->QueryStringAttribute("semantic",  &semantic)  == TIXML_SUCCESS);
	assert(input->QueryStringAttribute("source",    &source)    == TIXML_SUCCESS);
	assert(!strcasecmp(semantic.c_str(), "POSITION"));
		
	source_t _source = get_source(source);
	assert(sources.find(id) == sources.end());
	sources[id] = _source;
	
	printf("alias %s %s\n", source.c_str(), id.c_str());
}

void parse_polylist(TiXmlElement* polylist, geometry_t* geometry)
{
	mesh_t mesh;
	mesh.begin_index = geometry->verteces.size();
	const char* material = polylist->Attribute("material");
	if (material)
	{
		mesh.material = materials.at(material);
	}
	
	const char* value = polylist->Value();
	
	int count;
	assert(polylist->QueryIntAttribute("count", &count) == TIXML_SUCCESS);
	
	map<string, input_t> poly_inputs = parse_inputs(polylist);
	
	if (!strcasecmp(value, "polylist"))
	{
		TiXmlElement* vcount = polylist->FirstChildElement("vcount");
		assert(vcount);
		TiXmlNode* text = vcount->FirstChild();
		assert(text);
		const char* data = text->Value();		
		stringstream stream(data);
	
		for (int i = 0; i < count; i++)
		{
			int c;
			assert(stream >> c);
			assert(c == 3);
		}
	}
	
	TiXmlElement* p = polylist->FirstChildElement("p");
	assert(p);
	TiXmlNode* text = p->FirstChild();
	assert(text && text->Type() == TiXmlNode::TINYXML_TEXT);
	const char* data = ((TiXmlText*) text)->Value();
	stringstream stream(data);
	
	vector<int> indeces;
	int I;
	while (stream >> I)
		indeces.push_back(I);
	
	//=======================
	input_t& vertex_input   = poly_inputs.at("vertex");
	input_t& normal_input   = poly_inputs.at("vertex");
	mesh.texture = (bool) poly_inputs.count("texcoord");
	input_t* texcoord_input = NULL;
	if (mesh.texture)
		texcoord_input = &poly_inputs.at("texcoord");
	
	for (int i = 0; i < count * 3; i++)
	{
		int vertex_index   = indeces[i * indeces.size() / (count * 3) + vertex_input.offset];
		int normal_index   = indeces[i * indeces.size() / (count * 3) + normal_input.offset];
		int texcoord_index = mesh.texture ? (indeces[i * indeces.size() / (count * 3) + texcoord_input->offset]) : -1;
		geometry->indeces.push_back(vertex_index);
		
		vertex_t vertex;
		vertex.Position.x = get_param_float(vertex_input.source,   vertex_index,   "x");
		vertex.Position.y = get_param_float(vertex_input.source,   vertex_index,   "y");
		vertex.Position.z = get_param_float(vertex_input.source,   vertex_index,   "z");
		vertex.Normal.x   = get_param_float(vertex_input.source,   normal_index,   "x");
		vertex.Normal.y   = get_param_float(vertex_input.source,   normal_index,   "y");
		vertex.Normal.z   = get_param_float(vertex_input.source,   normal_index,   "z");
		vertex.Normal     = vertex.Normal.Normalize();
		
		if (mesh.texture)
		{
			vertex.TexCoord.x = get_param_float(texcoord_input->source, texcoord_index, "s");
			vertex.TexCoord.y = get_param_float(texcoord_input->source, texcoord_index, "t");
		}
		
		geometry->verteces.push_back(vertex);
	}
	
	mesh.end_index = geometry->verteces.size();
	geometry->meshes.push_back(mesh);
}

void parse_geometry(TiXmlElement* geometry)
{
	string id;
	assert(geometry->QueryStringAttribute("id",  &id) == TIXML_SUCCESS);
	assert(geometries.find(id) == geometries.end());
	
	geometry_t& geometry_data = geometries[id];

	TiXmlElement* mesh = geometry->FirstChildElement("mesh");
	assert(mesh);
	
	for (TiXmlElement* triangles = mesh->FirstChildElement("triangles"); triangles; triangles = triangles->NextSiblingElement("triangles"))
		parse_polylist(triangles, &geometry_data);
	for (TiXmlElement* polylist  = mesh->FirstChildElement("polylist");  polylist;  polylist  = polylist->NextSiblingElement("polylist"))
		parse_polylist(polylist, &geometry_data);
}
