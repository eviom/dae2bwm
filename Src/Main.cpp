#include "Geometry.h"
#include "Animation.h"
#include "Export.h"

int main(int argc, char** argv)
{
	char input[80]       = "LegoMan.dae";
	char output[80]      = "/mnt/data/BlocksWorld/Files/LegoMan.bwm";
	char animations[80]  = "animations.txt";
	texture = "LegoMan.png";
	/*
	char output[80];
	
	if (argc == 1)
	{
		printf("Input file name:\n");
		gets(input);
		printf("Output file name:\n");
		gets(output);
	}
	else
	{
		assert(argc == 4);
		strcpy(input,  argv[1]);
		texture = argv[2];
		strcpy(output, argv[3]);
	}
	*/
	
	TiXmlDocument doc(input);
	assert(doc.LoadFile());
	
	out.Init(output, CFILE_WRITE | CFILE_BINARY | CFILE_CREATE);
	assert(out);
	
	TiXmlElement* COLLADA = doc.FirstChildElement("COLLADA");
	assert(COLLADA);
	parse_all(COLLADA, "effect",      parse_effect);
	parse_all(COLLADA, "material",    parse_material);
	parse_all(COLLADA, "NAME_array",  parse_NAME_array);
	parse_all(COLLADA, "float_array", parse_float_array);
	parse_all(COLLADA, "source",      parse_source);
	parse_all(COLLADA, "vertices",    parse_vertices);
	parse_all(COLLADA, "geometry",    parse_geometry);
	
	find_root_joint(COLLADA);	
	assert(root_joint);	
	read_skeleton(root_joint, -1);
	
	
	animation.resize(joints.size());
	
	parse_all(COLLADA, "skin",        parse_skin);
	parse_all(COLLADA, "sampler",     parse_sampler);
	parse_all(COLLADA, "channel",     parse_channel);
	
	write_geometries();
	write_joints();
	write_animations(animations);
	
	out << Model;
	
	out.Destroy();
	
	return 0;
}
