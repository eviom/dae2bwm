#include "Common.h"

void parse_float_array(TiXmlElement* array)
{
	string id;
	int count = 0;
	const char* data = NULL;
	
	get_array_data(array, &id, &count, &data);
	stringstream stream(data);
	vector<float>& dest = float_arrays[id];
	dest.resize(count);
	
	for (int i = 0; i < count; i++)
	{
		assert(stream >> dest[i]);
	}
}

void parse_NAME_array(TiXmlElement* array)
{
	string id;
	int count = 0;
	const char* data = NULL;
	
	get_array_data(array, &id, &count, &data);
	stringstream stream(data);
	vector<string>& dest = NAME_arrays[id];
	dest.resize(count);
	
	for (int i = 0; i < count; i++)
	{
		assert(stream >> dest[i]);
	}
}

CMatrix<4, 4> read_matrix(const char* data)
{
	CMatrix<4, 4> matrix;
	stringstream stream(data);
	
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
		{
			assert(stream >> matrix(i, j));
		}
		
	for (int j = 0; j < 4; j++)
	{
		assert(matrix(3, j) == ((j == 3) ? 1 : 0));
	}
		
	return matrix;
}

map<string, input_t>  parse_inputs(TiXmlElement* data, bool offset)
{
	map<string, input_t> inputs;
	
	for (TiXmlElement* _input = data->FirstChildElement("input"); _input; _input = _input->NextSiblingElement("input"))
	{
		input_t input;
		string semantic;
			
		assert(_input->QueryStringAttribute("source",   &input.source)   == TIXML_SUCCESS);
		assert(_input->QueryStringAttribute("semantic", &semantic) 	     == TIXML_SUCCESS);
		
		if (offset)
		{
			assert(_input->QueryIntAttribute   ("offset",   &input.offset)   == TIXML_SUCCESS);
		}
		
		std::transform(semantic.begin(), semantic.end(), semantic.begin(), ::tolower);
		assert(inputs.find(semantic) == inputs.end());
		inputs[semantic] = input;
	}
	
	return inputs;
}

void parse_all(TiXmlNode* node, const char* name, void (*handler)(TiXmlElement*))
{
	for (TiXmlNode* child = node->FirstChild(); child; child = child->NextSibling())
	{
		if (child->Type() == TiXmlNode::TINYXML_ELEMENT && !strcasecmp(child->Value(), name))
			handler((TiXmlElement*) child);
		else
			parse_all(child, name, handler);
	}
}

void parse_source(TiXmlElement* source)
{
	const char* id = source->Attribute("id");
	if (!id)
		return;
	assert(id);
	assert(sources.find(id) == sources.end());
	
	source_t& data = sources[id];
	TiXmlElement* technique_common = source->FirstChildElement("technique_common");
	assert(technique_common);
	
	TiXmlElement* accessor = technique_common->FirstChildElement("accessor");
	assert(accessor);
	
	const char* _source = accessor->Attribute("source");
	assert(_source);
	data.source = _source;
	assert(accessor->QueryIntAttribute("count",  &data.count)  == TIXML_SUCCESS);
	assert(accessor->QueryIntAttribute("stride", &data.stride) == TIXML_SUCCESS);
	
	int param_offset = 0;
	for (TiXmlElement* param = accessor->FirstChildElement("param"); param; param = param->NextSiblingElement("param"))
	{
		string name;
		string type;
		
		bool has_name = (param->QueryStringAttribute("name", &name) == TIXML_SUCCESS);
		
		if (has_name)
		{
			assert(param->QueryStringAttribute("type", &type) == TIXML_SUCCESS);
			
			param_t new_param;
			new_param.type   = type;
			new_param.offset = param_offset;
			
			std::transform(name.begin(), name.end(), name.begin(), ::tolower);
			assert(data.params.find(name) == data.params.end());
			data.params[name] = new_param;
		}
		
		param_offset++;
	}
	
	//printf("\nsource id: %s source: %s stride: %d count: %d\n", id, data.source.c_str(), data.stride, data.count);
	//for (int i = 0; i < data.params.size(); i++)
	//	printf("\t%s %s\n", data.params[i].type.c_str(), data.params[i].name.c_str());
}
